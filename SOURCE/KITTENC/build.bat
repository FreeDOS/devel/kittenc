::
:: WATCOM build
::
@echo off

set include=%watcom%\h

wcl kittenc.c kitten.c -k10000 -d0 

if errorlevel 1 goto END

UPX kittenc.exe


			:: depending on OS and compiler
			:: executables are write protected while they execute

copy kittenc.exe temp.exe /Y

kittenc temp.exe attach nls\kittenc.??

copy temp.exe kittenc.exe


:END