# kitten

catgets/kittengets compatible resource compiler

## Documentation

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

    KITTENC - catgets/kittengets compatible resource compiler

KITTENC 'compiles' language resources to strings and attaches
these strings to the executable program such that the program can
retrieve these strings at execution time. This way it is enough to
copy a file to another place WITHOUT taking care that the resources
(in the NLS directory) are also copied.

KITTENC has 2 basic modes of operation:

    KITTENC  fdisk.exe  ATTACH NLS\fdisk.DE

attaches the german resources to fdisk.exe, and makes german the
default language for fdisk.exe.


    KITTENC  fdisk.exe  ATTACH NLS\fdisk.??

attaches the all resources to fdisk.exe, and then requires the definition of

    SET LANG=DE

if you want german messages.

this saves a lot of disk space compared old KITTEN/NLS approach as everything
is stored in a single file with cluster slack exactly once, not once for each
message file. also, the new runtime KITTEN.C is 600 byte smaller then the original
as all the intelligence, parsing etc. are done at compile time.

it's implementation also saves a few byte for each message translation call bcause clever;)


USAGE:

replace old KITTEN.H by new KITTEN.H
replace old KITTEN.C by new KITTEN.C

add something similar to your main() function

    main(int argc, char *argv[])
    {
	    {
    	static char nls_buffer[4000];

	    kittenopen(argv[0], nls_buffer, sizeof(nls_buffer));
	}

so kitten has some room to store messages and not forcing to
call malloc().

compile

add as last step

    KITTENC  fdisk.exe  ATTACH NLS\fdisk.??

   where NLS\fdisk.?? are the aasumed language resources for your program

to your build instructions.

that should be it.

now
    set LANG=ES
and enjoy your spanish translation

********* building KITTENC.EXE  ***********

to build DOS KITTENC.EXE, run BUILD.BAT for a watcom executable

for a WINDOWS executable (and easier developing) start KITTENC.DSW
with MS Visual Studio or compatible environment.

however it should be easy to port to a diffenet environment as the process is as complicated as

	wcl kittenc.c kitten.c -k10000 -d0
	if errorlevel 1 goto END
			:: depending on OS and compiler
			:: executables are write protected while they execute

	copy kittenc.exe temp.exe /Y
	kittenc temp.exe attach nls\kittenc.??
	copy temp.exe kittenc.exe


Tom






















